# remove_obsolete_packages

This single bash script moves the obsoleted packages from a pkgsrc repository to a subdirectory 'OBSOLETE'.