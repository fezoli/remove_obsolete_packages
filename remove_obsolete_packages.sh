#!/usr/bin/env bash

PACKAGES_DIR='/usr/pkgsrc/packages/All'
OBSOLETE_DIR="$PACKAGES_DIR/OBSOLETE"

FILES=`ls $PACKAGES_DIR`
COUNT=`echo $FILES |  wc -w | sed 's/[ ]*//'`

previous_file_start=' '
actual_file_start=' '
obsolete_file=''

i=1

if [ ! -d "$OBSOLETE_DIR" ]
then
    echo "$OBSOLETE_DIR does not exist. Creating it..."
    mkdir $OBSOLETE_DIR
fi


for file in $FILES;
do
    actual_file_start=`echo $file | sed 's/-[0-9].*$/ /'`

    printf "[$i/$COUNT] Checking $file\n"

    if [ "$actual_file_start" == "$previous_file_start" ];
    then
        obsolete_file=$previous_file;
    else
        obsolete_file='';
    fi


    if [ ! -z $obsolete_file ];
    then
        #echo "mv -v $PACKAGES_DIR/$file $OBSOLETE_DIR"
        mv -v "$PACKAGES_DIR/$file" $OBSOLETE_DIR
        echo;
    fi

    previous_file_start=`echo $file | sed 's/-[0-9].*$/ /'`;
    previous_file=$file
    let "i=i+1"

done;

exit 0

